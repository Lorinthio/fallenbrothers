﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ben.FallenBrothers.Dto
{
    public partial class AccountDto
    {
        public int AccountId { get; set; } // AccountId (Primary key)
        [Required]
        public string Username { get; set; } // Username (length: 50)
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; } // Password (length: 50)
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; } // Email (length: 50)
        public System.DateTime? CreatedOn { get; set; } // CreatedOn
    }
}
