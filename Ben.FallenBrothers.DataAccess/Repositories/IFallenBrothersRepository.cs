﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ben.FallenBrothers.DataAccess.Repositories
{
    public interface IFallenBrothersRepository: IDisposable
    {
        TModelEntity Get<TModelEntity, TDataAccessEntity>(Guid customerId) where TDataAccessEntity : class;
        TModelEntity Get<TModelEntity, TDataAccessEntity>(Func<TDataAccessEntity, bool> p) where TDataAccessEntity : class;
        IEnumerable<X> GetAllForEnumeration<X>();
        IEnumerable<TModelEntity> GetAllWhere<TModelEntity, TDataAccessEntity>(Func<TDataAccessEntity, bool> p) where TDataAccessEntity : class;
        Task InsertOrUpdate<TModelEntity, TDataAccessEntity>(TModelEntity insert, int Id) where TDataAccessEntity : class;
    }
}
