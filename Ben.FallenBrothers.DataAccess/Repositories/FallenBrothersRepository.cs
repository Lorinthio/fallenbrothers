﻿using AutoMapper;
using Ben.FallenBrothers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ben.FallenBrothers.DataAccess.Repositories
{
    public class FallenBrothersRepository : IFallenBrothersRepository, IDisposable
    {
        private IFallenBrothersContext _context;

        public FallenBrothersRepository() : this(new FallenBrothersContext()) {
            _context = new FallenBrothersContext();
        }

        public FallenBrothersRepository(IFallenBrothersContext fallenBrothersContext)
        {
            _context = fallenBrothersContext;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public TModelEntity Get<TModelEntity, TDataAccessEntity>(Guid customerId) where TDataAccessEntity : class
        {
            TDataAccessEntity result = _context.Set<TDataAccessEntity>()?.Find(customerId);

            if(result != null)
                return Mapper.Map<TDataAccessEntity, TModelEntity>(result);
            return default(TModelEntity);
        }


        public TModelEntity Get<TModelEntity, TDataAccessEntity>(Func<TDataAccessEntity, bool> p) where TDataAccessEntity : class
        {
            TDataAccessEntity result = _context.Set<TDataAccessEntity>().FirstOrDefault(p);

            return Mapper.Map<TDataAccessEntity, TModelEntity>(result);
        }

        public IEnumerable<TModelEntity> GetAllWhere<TModelEntity, TDataAccessEntity>(Func<TDataAccessEntity, bool> p) where TDataAccessEntity : class
        {
            IEnumerable<TDataAccessEntity> result = _context.Set<TDataAccessEntity>().Where(p).ToList();

            return Mapper.Map<IEnumerable<TDataAccessEntity>, IEnumerable<TModelEntity>>(result);
        }

        public IEnumerable<X> GetAllForEnumeration<X>()
        {

            return null;
        }

        public async Task InsertOrUpdate<TModelEntity, TDataAccessEntity>(TModelEntity insert, int Id) where TDataAccessEntity : class
        {
            TDataAccessEntity entity = Mapper.Map<TModelEntity, TDataAccessEntity>(insert);
            if(Id == 0)
            {
                _context.Set<TDataAccessEntity>().Add(entity);
                await _context.SaveChangesAsync();
            }
            else
            {
                var oldEntity = _context.Set<TDataAccessEntity>().Find(Id);
                if (oldEntity != null)
                {
                    _context.Entry(oldEntity).CurrentValues.SetValues(entity);
                    await _context.SaveChangesAsync();
                }
            }
            
        }
    }
}
