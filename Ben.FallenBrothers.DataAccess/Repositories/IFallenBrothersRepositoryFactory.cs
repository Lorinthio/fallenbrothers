﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ben.FallenBrothers.DataAccess.Repositories
{
    public interface IFallenBrothersRepositoryFactory
    {
        IFallenBrothersRepository Create();
    }
}
