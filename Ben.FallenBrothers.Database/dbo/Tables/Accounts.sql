﻿CREATE TABLE [dbo].[Accounts]
(
	[AccountId] INT NOT NULL UNIQUE IDENTITY PRIMARY KEY, 
    [Username] VARCHAR(50) NOT NULL, 
    [Password] VARCHAR(50) NOT NULL,
	[Email] VARCHAR(50) NULL,
    [CreatedOn] DATETIME DEFAULT (getdate()) NULL
)
