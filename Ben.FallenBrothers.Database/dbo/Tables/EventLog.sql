﻿CREATE TABLE [dbo].[EventLog] (
    [LogId]        UNIQUEIDENTIFIER CONSTRAINT [DF_Log_LogId] DEFAULT (newid()) NOT NULL,
    [Level]        NVARCHAR (50)    NOT NULL,
    [Logger]       NVARCHAR (128)   NOT NULL,
    [ThreadId]     NVARCHAR (50)    NOT NULL,
    [Exception]    NVARCHAR (128)   NULL,
    [Content]      NVARCHAR (MAX)   NULL,
    [StackTrace]   NVARCHAR (MAX)   NULL,
    [Date_Created] DATETIME2 (7)    CONSTRAINT [DF_Log_Date_Created] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED ([LogId] ASC)
);