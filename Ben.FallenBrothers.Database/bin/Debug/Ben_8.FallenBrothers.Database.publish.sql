﻿/*
Deployment script for Ben.FallenBrothers.Database

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "Ben.FallenBrothers.Database"
:setvar DefaultFilePrefix "Ben.FallenBrothers.Database"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
The column [dbo].[Accounts].[Email] on table [dbo].[Accounts] must be added, but the column has no default value and does not allow NULL values. If the table contains data, the ALTER script will not work. To avoid this issue you must either: add a default value to the column, mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/

IF EXISTS (select top 1 1 from [dbo].[Accounts])
    RAISERROR (N'Rows were detected. The schema update is terminating because data loss might occur.', 16, 127) WITH NOWAIT

GO
PRINT N'Starting rebuilding table [dbo].[Accounts]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Accounts] (
    [AccountId] INT          IDENTITY (1, 1) NOT NULL,
    [Username]  VARCHAR (50) NOT NULL,
    [Password]  VARCHAR (50) NOT NULL,
    [Email]     VARCHAR (50) NOT NULL,
    [CreatedOn] DATETIME     NULL,
    PRIMARY KEY CLUSTERED ([AccountId] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Accounts])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Accounts] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Accounts] ([AccountId], [Username], [Password], [CreatedOn])
        SELECT   [AccountId],
                 [Username],
                 [Password],
                 [CreatedOn]
        FROM     [dbo].[Accounts]
        ORDER BY [AccountId] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Accounts] OFF;
    END

DROP TABLE [dbo].[Accounts];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Accounts]', N'Accounts';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

if not exists(select * from Accounts where AccountId = 0) Insert into Accounts (Username, Password, Email, CreatedOn) Values ('lorinthio', 'Lorinthy1', 'bnra226@g.uky.edu', GETDATE())
GO

GO
PRINT N'Update complete.';


GO
