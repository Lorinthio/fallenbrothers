﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ben.FallenBrothers.DataAccess;
using Ben.FallenBrothers.DataAccess.Repositories;
using Ben.FallenBrothers.Dto;

namespace Ben.FallenBrothers.Logic
{
    public class AccountManager
    {

        private readonly IFallenBrothersRepositoryFactory fallenBrothersRepositoryFactory;

        public AccountManager() : this(new FallenBrothersRepositoryFactory()) { }
        public AccountManager(IFallenBrothersRepositoryFactory factory)
        {
            fallenBrothersRepositoryFactory = factory;
        }

        public AccountDto Get(int id)
        {
            if(id > 0)
            {
                using(var repo = fallenBrothersRepositoryFactory.Create())
                {
                    return repo.Get<AccountDto, Account>(x => x.AccountId == id);
                }
            }
            return null;
        }

        public bool CheckLogin(string user, string password)
        {
            if(!string.IsNullOrWhiteSpace(user) && !string.IsNullOrWhiteSpace(password))
            {
                using(var repo = fallenBrothersRepositoryFactory.Create())
                {
                    var account = repo.Get<AccountDto, Account>(x => x.Username == user);
                    if (account != null)
                        return account.Password == password;
                }
            }
            return false;
        }

        //Returns true/false on success/failure
        public async Task<bool> CreateAccount(string user, string password, string email)
        {
            if (!string.IsNullOrWhiteSpace(user) && !string.IsNullOrWhiteSpace(password) && !string.IsNullOrWhiteSpace(email))
            {
                using (var repo = fallenBrothersRepositoryFactory.Create())
                {
                    //Don't allow similar username or email
                    var account = repo.Get<AccountDto, Account>(x => x.Username == user || x.Email == email);
                    if (account == null)
                    {
                        var newAccount = new AccountDto()
                        {
                            AccountId = -1,
                            Username = user.ToLower(),
                            Password = password,
                            Email = email,
                            CreatedOn = DateTime.Now
                        };
                        await repo.InsertOrUpdate<AccountDto, Account>(newAccount, 0);
                        return true;
                    }
                    throw new Exception("Account already exists with that username/password!");
                }
            }
            if(string.IsNullOrWhiteSpace(user))
                throw new Exception("You are missing a username");
            if (string.IsNullOrWhiteSpace(password))
                throw new Exception("You are missing a password");
            if (string.IsNullOrWhiteSpace(email))
                throw new Exception("You are missing an email");

            return false;
        }
    }
}
