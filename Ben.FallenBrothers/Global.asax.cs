﻿using Ben.FallenBrothers.App_Start;
using NLog;
using NLog.Targets;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Ben.FallenBrothers
{
    public class MvcApplication : HttpApplication
    {

        private static ILogger logger = LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            AutoMapperConfig.Configure();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ConfigureLogging();
            logger.Info("Application Start");
        }

        private static void ConfigureLogging()
        {
            var target = (DatabaseTarget)LogManager.Configuration.FindTargetByName("Database");
            target.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DefaultConnection"];
            LogManager.ReconfigExistingLoggers();
        }

        protected virtual void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            var correlationId = Guid.NewGuid();

            logger.Fatal(ex, correlationId.ToString());
        }
    }
}
