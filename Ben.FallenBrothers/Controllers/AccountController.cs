﻿using Ben.FallenBrothers.Dto;
using Ben.FallenBrothers.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Ben.FallenBrothers.Controllers
{
    
    public class AccountController : ApiController
    {
        private AccountManager _accountManager;

        public AccountController()
        {
            _accountManager = new AccountManager();
        }

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        [Route("account/{id}")]
        [ResponseType(typeof(AccountDto))]
        public AccountDto Get(int id)
        {
            return _accountManager.Get(id);
        }

        [HttpPost]
        [Route("account/login/{username}/{password}")]
        [ResponseType(typeof(bool))]
        public bool Login(string username, string password)
        {
            return _accountManager.CheckLogin(username, password);
        }

        [HttpPost]
        [Route("account/create")]
        [ResponseType(typeof(bool))]
        public async Task<bool> Create([FromBody] AccountDto account)
        {
            return await _accountManager.CreateAccount(account.Username, account.Password, account.Email);
        }
    }
}