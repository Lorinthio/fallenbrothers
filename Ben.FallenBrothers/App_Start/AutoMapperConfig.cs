﻿using AutoMapper;
using Ben.FallenBrothers.DataAccess;
using Ben.FallenBrothers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ben.FallenBrothers.App_Start
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<AccountDto, Account>().ReverseMap();
            });
            Mapper.Configuration.CompileMappings();
        }
    }
}